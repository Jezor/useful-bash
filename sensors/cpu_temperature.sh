#!/bin/bash

# Help information

help_message='Simple script for getting CPU temperature

Options
    -h - Show this message and exit.
    -m - Print temperature in mili degrees celsius (default).
    -c - Print temperature in degrees celsius.
    -f - Print temperature in degrees fahrenheit.
    -k - Print temperature in kelvins.
    -v - Print degree symbol.'

print_milicelsius="1"
print_celsius="0"
print_fahrenheit="0"
print_kelvin="0"
print_degree="0"

# Parsing arguments
OPTIND=1 # Reset getopts
while getopts "h?:mcfkv" opt; do
    case "$opt" in
    h|\?) echo -e "$help_message"; exit 0;;
    m) print_milicelsius="1";print_celsius="0";print_fahrenheit="0";print_kelvin="0";;
    c) print_milicelsius="0";print_celsius="1";print_fahrenheit="0";print_kelvin="0";;
    f) print_milicelsius="0";print_celsius="0";print_fahrenheit="1";print_kelvin="0";;
    k) print_milicelsius="0";print_celsius="0";print_fahrenheit="0";print_kelvin="1";;
    v) print_degree="1";;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

milicelsius=$(cat /sys/class/thermal/thermal_zone0/temp)
celsius=$(bc <<< "$milicelsius / 1000")
fahrenheit=$(bc <<< "($celsius * 9/5) + 32")
kelvin=$(bc <<< "$celsius + 273.15")

if [[ "$print_milicelsius" = "1" ]]; then
    echo -n "$milicelsius"
    if [[ "$print_degree" = "1" ]]; then
        echo " °mC"
    else
        echo
    fi
fi

if [[ "$print_celsius" = "1" ]]; then
    echo -n "$celsius"
    if [[ "$print_degree" = "1" ]]; then
        echo " °C"
    else
        echo
    fi
fi

if [[ "$print_fahrenheit" = "1" ]]; then
    echo -n "$fahrenheit"
    if [[ "$print_degree" = "1" ]]; then
        echo " °F"
    else
        echo
    fi
fi

if [[ "$print_kelvin" = "1" ]]; then
    echo -n "$kelvin"
    if [[ "$print_degree" = "1" ]]; then
        echo " K"
    else
        echo
    fi
fi

